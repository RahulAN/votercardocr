# C:\Users\rahjain\Documents\FunProjects\OCR\Test\outcrop0.png
# X * n/100 = Y
# 103424 * n = 3220.0
# 322000/103424

import cv2
from random import seed
from random import randint

def getCroppedPix(apx):
    x, y = [] , []
    for i in apx:
        x.append(i[0][0])
        y.append(i[0][1])

    x1, x2, y1, y2 = min(x), max(x), min(y), max(y)
    return [x1, x2, y1, y2]

def resize(img):
    scale_percent = 300 # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    cv2.imshow("resized", resized)

def openImage():
    file = "C:\\Users\\rahjain\\Documents\\FunProjects\\OCR\\Test\\outcrop0.png"
    img = cv2.imread(file)
    # img = cv2.resize(img, (0,0), fx=0.5, fy=0.5)
    cv2.imshow("org", img)
    resize(img)
    res = img.shape[0] * img.shape[1]
    print res
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # title = processTitle(gray)

    # Threshold Increase from 127 to 200
    ret,thresh = cv2.threshold(gray,200,255,1)
    # thresh = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
    (_,contours,_) = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        approx = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True)

        if len(approx)==4:
            area = cv2.contourArea(cnt)
            print area
            if area > res*0.03:
                crp_img_coord = getCroppedPix(approx)
                cropped = img[crp_img_coord[2]:crp_img_coord[3], crp_img_coord[0]:crp_img_coord[1]]
                cv2.drawContours(img,[cnt],0,(255, 255, 255),3)

    cv2.imshow("hello", img)


    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    openImage()
