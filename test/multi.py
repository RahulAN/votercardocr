from multiprocessing import Process, Lock
import pytesseract
import cv2
 
 
def greeting(l, i):
    l.acquire()
    for i in range(0,100):
    	print 'hello', i
    l.release()

def processTitle(lock, name):
    img = cv2.imread("../Data/RJ_hin/outputs/RJ_hin_10_179.jpg")
    height, width = img.shape[:2]
    x,y,w,h = (1, 6, width, 122)
    # crop image
    rect  = img[y:y+h, x:x+w]
    ret = getVoterData(rect)
    print "processing Image " + str(name)

def getVoterData(img):
    a = pytesseract.image_to_string(img, lang='hin+eng')
    # a = pytesseract.image_to_string(img, lang='eng')
    l = a.split("\n")
    text = ""
    for i in l:
        if i:
            text = text + " " +  i.encode("utf-8")
    text = text.strip()
    text = text.replace(",", " ")
    return " ".join(text.split())


 
if __name__ == '__main__':
    lock = Lock()

    for name in range(0, 5):
        #Process(target=greeting, args=(lock, name)).start()
        #Process(target=processTitle, args=(lock, name)).start()
	processTitle(lock, name)
