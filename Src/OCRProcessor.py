import pytesseract
import cv2
import os
import multiprocessing

try:
    from PIL import Image
except ImportError:
    import Image

import openpyxl

os.environ['OMP_THREAD_LIMIT'] = str(multiprocessing.cpu_count())
wb = openpyxl.Workbook()
sheet = wb.active
# img = cv2.imread("../Test/outcrop0.png")

# print pytesseract.image_to_string(Image.open("../Test/OCR_test1.jpg"), lang='hin')

def processTitle():
    img = cv2.imread("bin/MP_hin/pdfs/MP_hin_input_img_3.jpg")
    height, width = img.shape[:2]
    print height
    print width
    x,y,w,h = (1, 6, width, 122)

    # crop image
    rect  = img[y:y+h, x:x+w]
    # cv2.imshow("Rect", rect)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    print getText(rect, "Hello")

def getText(img, file):
    a = pytesseract.image_to_string(img, lang='hin+eng')
    l = a.split("\n")
    text = ""
    for i in l:
        if i:
            text = text + i.encode("utf-8")
    addCell(1,2, text)
    return text


def addCell(row, col, value):
    cell = sheet.cell(row, col)
    cell.value = value

processTitle()
wb.save("./demo.xlsx")
