#
# This File is responsible for converting pdf to Image.
# Process Image to get all Voter's List cropped images
# Do OCR
# Fill Excel sheet
# @author Rahul Jain
#

import sys
import pytesseract
import os
import multiprocessing
import shutil
import time

try:
    from PIL import Image
except ImportError:
    import Image

os.environ['OMP_THREAD_LIMIT'] = str(multiprocessing.cpu_count())

verbose = 0
g_counter = 0 # This counter will be used in creating croped images
##################### Image Processing Implementation ########################

import cv2
import numpy as np
from random import seed
from random import randint
# seed random number generator
seed(1)

def showImage(text, img):
    print ("Showing " + text)
    cv2.imshow(text, img)
    pass

def getCroppedPix(apx):
    x, y = [] , []
    for i in apx:
        x.append(i[0][0])
        y.append(i[0][1])

    x1, x2, y1, y2 = min(x), max(x), min(y), max(y)
    return [x1, x2, y1, y2]

def tuneImageQuality(img):
    res = img.shape[0] * img.shape[1]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Threshold Increase from 127 to 200
    ret,thresh = cv2.threshold(gray,200,255,1)
    (_,contours,_) = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        approx = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True)

        if len(approx)==4:
            area = cv2.contourArea(cnt)
            if area > res*0.03:
                cv2.drawContours(img,[cnt],0,(255, 255, 255),3)
    return img

def openImage(file, outputDir, fileName, page_number):
    global g_counter
    img = cv2.imread(file)
    res = img.shape[0] * img.shape[1]

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Threshold Increase from 127 to 200
    ret,thresh = cv2.threshold(gray,200,255,1)
    # thresh = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
    (_,contours,_) = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    count = g_counter
    oldCount = g_counter
    matImageArr = []
    for cnt in contours:
        approx = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True)
        if len(approx)==4:
            area = cv2.contourArea(cnt)

            if area > res*0.02 and area < res*0.03:
                crp_img_coord = getCroppedPix(approx)
                cropped = img[crp_img_coord[2]:crp_img_coord[3], crp_img_coord[0]:crp_img_coord[1]]

                tunedImage = tuneImageQuality(cropped)
                matImageArr.append(tunedImage)
                count += 1
                # cv2.drawContours(img,[cnt],0,(randint(0, 255),randint(0, 255),randint(0, 255)),3)
    # cv2.imwrite("test.jpg", img)
    # Reverse the array to get the correct count of Images
    matImageArr.reverse()

    # Iterate matImageArr and create files
    for idx,idx_count in enumerate(range(oldCount, count)):
        fName = outputDir + "/" + fileName + "_" + str(page_number) + "_" + str(idx_count) + ".jpg"
        print fName
        cv2.imwrite(fName, matImageArr[idx])

#    cv2.waitKey(0)
#    cv2.destroyAllWindows()
    g_counter = count

###############################################################################

######################### EXCEL Implementation ################################
def addtoCsv(text, fo):
    text = text + "\n"
    fo.write(text)

###############################################################################

######################### PDF Convertor Implementation ########################

from pdf2image import convert_from_path
import os

def getFilenameForImage(count, pdf):
    # pdf name will come from command line
    m_pdf = pdf.split("/").pop().strip(".pdf")
    return m_pdf + "_input_img_" + str(count) + ".jpg"

def getFilename(pdf):
    return pdf.split("/").pop().strip(".pdf")

def getFilePath(pdf):
    path = pdf.replace(pdf.split("/").pop(), "")
    if len(path) > 1:
        return path
    else :
        return "."

def convertToImage(pdfFile, outputDir="."):
    global g_counter

    outputDir = getFilePath(pdfFile)

    count = 0
    dir = outputDir + getFilename(pdfFile) + "/pdfs/"
    croppedDir = outputDir + getFilename(pdfFile) + "/outputs/"


    try:
        # Create Dir if not exists
        if not os.path.exists(dir):
            debug("Creating Directory for PDF Images : " + dir)
            os.makedirs(dir, 0755)

        if not os.path.exists(croppedDir):
            debug("Creating Directory for Cropped Images : " + croppedDir)
            os.makedirs(croppedDir, 0755)

        pages = convert_from_path(pdfFile, dpi=150, output_folder=dir, fmt='jpg', thread_count=4) #('../Data/test.pdf')
        for page in pages:
            filename = getFilenameForImage(count, pdfFile)
            imgFile = dir + "/" + filename
            # It means that it will save from page 3
            # As discussed the data starts from page 3
            if count > 1:
                print (page.filename)
                openImage(page.filename, croppedDir, getFilename(pdfFile), count)

            count += 1
        # exit()
        # Reset Global counter
        g_counter = 0
        # Save to workbook
        print ("Deleting " + dir)
        try:
            shutil.rmtree(dir)
        except Exception as e:
            print (e)
    except Exception as e:
        print (e)
        shutil.rmtree(outputDir + getFilename(pdfFile), ignore_errors=True)
        print "Error : Unable to Open PDF File, Please Verify If file is correct and not corrupted?, Skipping ..."
        pass


def getAllPdfsName(dirName):
    if os.path.isdir(dirName):
        # files = [f for f in os.listdir(dirName) if f.endswith('.pdf')]
        files = []

        for f in os.listdir(dirName):
            if f.endswith('.pdf'):
                fDir = dirName + "/" + f
                files.append(fDir)

        for file in files:
            debug("converting PDF to Image : " + file)
            convertToImage(file, ".")


###############################################################################

def debug(string):
    if verbose:
        print string

def printUsage():
    print "Usage"
    print "For multiple Pdfs processing"
    print "ImageProcessor.py -d <folders which have pdfs>"
    print "For single Pdf processing"
    print "ImageProcessor.py -p <path to pdf>"

#
# main function
# check if the params are given or not else provide __error msg__
def main():
    print len(sys.argv)
    if len(sys.argv) < 2:
        printUsage()
        exit(1)

    # global verbose
    verbose = 1
    isDir = False

    if sys.argv[1] == "-d":
        # Parse Directory
        isDir = True
        getAllPdfsName(sys.argv[2])
        pass
    elif sys.argv[1] == "-p":
        # Parse Pdf
        convertToImage(sys.argv[2])
        isDir = False
        pass
    else :
        printUsage()

if __name__ == '__main__':
    main()
#    openImage("../Data/MP_hin/pdfs/d9506ad9-0973-40e8-82c6-6a2f2c5c750f-29.jpg", "../Test", "test", 1)
