#
# This Lib is responsible for converting pdf to Image.
# @author Rahul Jain
#

from pdf2image import convert_from_path
import os

def getFilename(count, pdf):
    # pdf name will come from command line
    m_pdf = pdf.split("/").pop().strip(".pdf")
    return m_pdf + "_input_img_" + str(count) + ".jpg"


def convertToImage(pdfFile, outputDir):
    pages = convert_from_path(pdfFile) #('../Data/test.pdf')
    count = 0
    dir = outputDir + "/inputPdf/" #"../Images/"

    # Create Dir if not exists
    if not os.path.exists(dir):
        os.makedirs(dir)

    filename = getFilename(count, pdf)

    for page in pages:
        imgFile = dir + "/" + filename
        # It means that it will save from page 3
        # As discussed the data starts from page 3
        if count > 1:
            page.save(imgFile, 'JPEG')

        count += 1
