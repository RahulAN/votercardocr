#
# This File is responsible for converting pdf to Image.
# Process Image to get all Voter's List cropped images
# Do OCR
# Fill Excel sheet
# @author Rahul Jain
#

import sys
import pytesseract
import os
from multiprocessing import Queue, Process, Lock, Pool
import shutil
import codecs
from openpyxl import Workbook

try:
    from PIL import Image
except ImportError:
    import Image

#os.environ['OMP_THREAD_LIMIT'] = str(multiprocessing.cpu_count())

queue = Queue()
mylist = []
lock = Lock()

verbose = 0
g_counter = 0 # This counter will be used in creating croped images
##################### Image Processing Implementation ########################

import cv2
import numpy as np
from random import seed
from random import randint
# seed random number generator
seed(1)

def showImage(text, img):
    print ("Showing " + text)
    cv2.imshow(text, img)
    pass

def getCroppedPix(apx):
    x, y = [] , []
    for i in apx:
        x.append(i[0][0])
        y.append(i[0][1])

    x1, x2, y1, y2 = min(x), max(x), min(y), max(y)
    return [x1, x2, y1, y2]

def tuneImageQuality(img):
    res = img.shape[0] * img.shape[1]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Threshold Increase from 127 to 200
    ret,thresh = cv2.threshold(gray,200,255,1)
    (_,contours,_) = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        approx = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True)
        if len(approx)==4:
            area = cv2.contourArea(cnt)
            if area > res*0.03:
                cv2.drawContours(img,[cnt],0,(255, 255, 255),3)
    return resize(img)

def resize(img):
    scale_percent = 300 # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    return resized

def openImage(file, outputDir, fileName, sheet, page_number):
    global g_counter
    matArr = []
    img = cv2.imread(file)
    res = img.shape[0] * img.shape[1]
    kernel = cv2.getStructuringElement(	cv2.MORPH_RECT, (3,3))
    img_morph =  cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
    gray = cv2.cvtColor(img_morph, cv2.COLOR_BGR2GRAY)

    # global g_counter
    # img = cv2.imread(file)
    # res = img.shape[0] * img.shape[1]
    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    title = processTitle(gray)
    # Threshold Increase from 127 to 200
    ret,thresh = cv2.threshold(gray,200,255,1)
    # thresh = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
    (_,contours,_) = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    # count = g_counter
    # for cnt in contours:
    #     approx = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True)
    #     if len(approx)==4:
    #         area = cv2.contourArea(cnt)
	#     if area > res*0.02 and area < res*0.03:
    #             matArrMetaData = {}
    #             crp_img_coord = getCroppedPix(approx)
    #             cropped = img[crp_img_coord[2]:crp_img_coord[3], crp_img_coord[0]:crp_img_coord[1]]
    #
    #             tunedImage = tuneImageQuality(cropped)
    #             fName = outputDir + "/" + fileName + "_" + str(page_number) + "_" + str(count) + ".jpg"
    #             matArrMetaData['fileName'] = fName
    #             matArrMetaData['data'] = cropped
    #             matArr.append(matArrMetaData)
    #
    #             cv2.imwrite(fName, tunedImage)
    #             count += 1

    count = g_counter
    oldCount = g_counter
    matImageArr = []
    for cnt in contours:
        approx = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True)
        # if len(approx)==6:
        area = cv2.contourArea(cnt)

        if area > res*0.01 and area < res*0.03:
            crp_img_coord = getCroppedPix(approx)
            cropped = img[crp_img_coord[2]:crp_img_coord[3], crp_img_coord[0]:crp_img_coord[1]]
            tunedImage = tuneImageQuality(cropped)
            matImageArr.append(tunedImage)
            count += 1
            # cv2.drawContours(img,[cnt],0,(randint(0, 255),randint(0, 255),randint(0, 255)),3)
    # cv2.imwrite("test.jpg", img)
    # Reverse the array to get the correct count of Images
    matImageArr.reverse()

    # Iterate matImageArr and create files
    for idx,idx_count in enumerate(range(oldCount, count)):
        matArrMetaData = {}
        fName = outputDir + "/" + fileName + "_" + str(page_number) + "_" + str(idx_count) + ".jpg"
        cv2.imwrite(fName, matImageArr[idx])
        matArrMetaData['fileName'] = fName
        matArrMetaData['data'] = matImageArr[idx]
        matArr.append(matArrMetaData)


    splitAndAddToProcess(matArr, 5, title, sheet)
    g_counter = count

###############################################################################

######################### Thread Implementation ###############################

def log_result(result):
    print "Adding Result"
    mylist.append(result)

def splitAndAddToProcess(a, n, Title, sheet):
    k, m = divmod(len(a), n)
    processList = (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in xrange(n))

    counter = 0
    procs = []
    print processList
    pool = Pool(5)
    for i in processList:
	from_cnt = counter
	counter = counter + (len(i)-1)
	to_cnt = counter
	pool.apply_async(ProcessOCR, args = (i, from_cnt, to_cnt, Title), callback = log_result)

    pool.close()
    pool.join()

    for i in mylist:
	addtoCsv(i, sheet)
    mylist[:] = []



def ProcessOCR(matArr, from_cnt, to_cnt, title):
    txt = ""
    for i in matArr:
	txt = txt + "\n" + title + "," + getVoterData(i['data']) + "," + i['fileName']
    return txt

###############################################################################

######################### OCR Implementation ##################################
def processTitle(img):
    # img = cv2.imread("bin/MP_hin/pdfs/MP_hin_input_img_3.jpg")
    height, width = img.shape[:2]
    x,y,w,h = (1, 6, width, 122)
    # crop image
    rect  = img[y:y+h, x:x+w]
    return getVoterData(rect)

def getVoterData(img):
    a = pytesseract.image_to_string(img, lang='hin+eng')
    # a = pytesseract.image_to_string(img, lang='eng')
    l = a.split("\n")
    text = ""
    for i in l:
        if i:
            text = text + " " +  i #.encode("utf-8")
    text = text.strip()
    text = text.replace(",", " ")
    return " ".join(text.split())

###############################################################################

######################### EXCEL Implementation ################################
def addtoCsv(text, fo):
    text = text + "\n"
    fo.write(text)

###############################################################################

######################### PDF Convertor Implementation ########################

from pdf2image import convert_from_path
import os

def getFilenameForImage(count, pdf):
    # pdf name will come from command line
    m_pdf = pdf.split("/").pop().strip(".pdf")
    return m_pdf + "_input_img_" + str(count) + ".jpg"

def getFilename(pdf):
    return pdf.split("/").pop().strip(".pdf")

def getFilePath(pdf):
    path = pdf.replace(pdf.split("/").pop(), "")
    if len(path) > 1:
        return path
    else :
        return "."

def convertToImage(pdfFile, outputDir="."):
    try:
        global g_counter

        outputDir = getFilePath(pdfFile)

        # Open Workbook
        csvFile = outputDir + getFilename(pdfFile) + ".csv"
        xlsxFile = outputDir + getFilename(pdfFile) + ".xlsx"

        wb = Workbook()
        ws1 = wb.active
        ws1.title = "Data"

        if os.path.isfile(csvFile):
            os.remove(csvFile)

        sheet = codecs.open(csvFile, "w", "utf-8")
        # sheet = open(csvFile, "w")


        count = 0
        dir = outputDir + getFilename(pdfFile) + "/pdfs/"
        croppedDir = outputDir + getFilename(pdfFile) + "/outputs/"

        # Create Dir if not exists
        if not os.path.exists(dir):
            debug("Creating Directory for PDF Images : " + dir)
            os.makedirs(dir, 0755)

        if not os.path.exists(croppedDir):
            debug("Creating Directory for Cropped Images : " + croppedDir)
            os.makedirs(croppedDir, 0755)

        pages = convert_from_path(pdfFile, dpi=150, output_folder=dir, fmt='jpg', thread_count=4)
        for page in pages:
            filename = getFilenameForImage(count, pdfFile)
            imgFile = dir + "/" + filename
            # It means that it will save from page 3
            # As discussed the data starts from page 3
            if count > 1:
                openImage(page.filename, croppedDir, getFilename(pdfFile), sheet, count)

            count += 1

        # Reset Global counter
        g_counter = 0
        # Save to workbook
        sheet.close()
        writeXlsFile(csvFile, wb, ws1)
        wb.save(filename = xlsxFile)

        print ("Deleting " + dir)
        try:
            shutil.rmtree(dir)
        except Exception as e:
            print (e)
    except Exception as e:
        print (e)
        try:
            shutil.rmtree(outputDir + getFilename(pdfFile), ignore_errors=True)
            print "Error : Unable to Open PDF File, Please Verify If file is correct and not corrupted?, Skipping ..."
        except Exception as _e:
            print (_e)
        pass

def getAllPdfsName(dirName):
    if os.path.isdir(dirName):
        # files = [f for f in os.listdir(dirName) if f.endswith('.pdf')]
        files = []

        for f in os.listdir(dirName):
            if f.endswith('.pdf'):
                fDir = dirName + "/" + f
                files.append(fDir)

        for file in files:
            debug("converting PDF to Image : " + file)
            convertToImage(file, ".")

def writeXlsFile(csvFile, wb, ws1):
    with open(csvFile) as f:
        lines = f.readlines()

    for rows in lines:
        cols = rows.split(",")
        ws1.append(cols)

###############################################################################

def debug(string):
    if verbose:
        print string

def printUsage():
    print "Usage"
    print "For multiple Pdfs processing"
    print "ImageProcessor.py -d <folders which have pdfs>"
    print "For single Pdf processing"
    print "ImageProcessor.py -p <path to pdf>"

#
# main function
# check if the params are given or not else provide __error msg__
def main():
    if len(sys.argv) < 2:
        printUsage()
        exit(1)

    # global verbose
    verbose = 1
    isDir = False

    if sys.argv[1] == "-d":
        # Parse Directory
        isDir = True
        getAllPdfsName(sys.argv[2])
        pass
    elif sys.argv[1] == "-p":
        # Parse Pdf
        convertToImage(sys.argv[2])
        isDir = False
        pass
    else :
        printUsage()

if __name__ == '__main__':
    main()
